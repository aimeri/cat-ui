import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Card from './components/Card'
import styled from 'styled-components'
import loading from './assets/loading.svg'

const Header = styled.span`
font-size: 30px; 
font-weight: bold; 
font-family: Verdana, Geneva, Tahoma, sans-serif;
margin: 20px;
padding: 20px;
border-radius: 10px; 
background-color: white;
color: rgba(68,0,153,0.5);
grid-area: header;
text-align: center;
`

const Loader = styled.div`
position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    }
  }
  componentDidMount() {
    fetch("https://cat-scraper-api.herokuapp.com/")
      .then(result => result.json())
      .then(data => this.setState({ data }));

  }
  render() {
    if (!this.state.data) {
      return <Loader><img src={loading} alt="Loading..." /></Loader>;
    }
    return (
      <React.Fragment>
        <Header>My Future Kitty</Header>
        {this.state.data.map((item, index) => 
          <Card key={index} cat={{
            name: item.Title,
            avatar: item.Pictures[0],
            description: item.Description,
            link: item.Link,
            location: item.Location,
            price: item.Price,
            posted: item.Posted,
            pictures: item.Pictures
          }} />
        )}
      </React.Fragment>

    );
  }
}