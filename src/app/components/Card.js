import React, { Component } from "react"
import { CardBackground, 
  CardFront, 
  Avatar, 
  PictureGalery, 
  StyledTooltip, 
  Name, 
  Info, 
  Description, 
  CatLink, 
  Posted } from './CardComponents'

export default function Card(props) {
  return (
      <CardBackground>

        <CardFront>
          <Avatar><img src={`https://res.cloudinary.com/slothcrew/image/upload/c_fill,g_face,h_200,r_max,w_200/v1542565154/${props.cat.avatar}.jpg`} /></Avatar>
          <Name>{props.cat.name}</Name>
          <Description>{props.cat.description}</Description>
          <Info>
              <p className="left"><span className="info">Price:</span> {props.cat.price}</p>
              <p className="right"><span className="info">Location:</span> {props.cat.location}</p>

          </Info>
          <CatLink>{props.cat.link}</CatLink>
          
          <PictureGalery>
            {props.cat.pictures.map((image, index) => (
              <img key={index} src={`https://res.cloudinary.com/slothcrew/image/upload/c_fill,g_face,h_50,w_50/v1542565154/${image}.jpg`} />
            )
            )}
          </PictureGalery>
        </CardFront>
        <Posted>Posted on {props.cat.posted}</Posted>
      </CardBackground>
  )
}
