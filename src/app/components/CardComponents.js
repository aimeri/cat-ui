import React, { Component } from "react"
import styled from "styled-components"
import Tooltip from '@material-ui/core/Tooltip';

const CardBackground = styled.div`
border-radius: 20px;
box-shadow: 0px 0px 20px -2px rgba(0,0,0,.4);
background-color: white;
padding: 25px;
margin: 20px 20px 20px 20px;
font-family: Verdana, Geneva, Tahoma, sans-serif;
position: relative;
`

const CardFront = styled.div`
display: grid;
justify-items: center;
grid-gap: 15px;
grid-template-columns: 100%;
grid-template-rows: auto;
/*
 */
grid-template-areas: 
'Avatar' 
'Name'
'Description'
'Info'
'Link'
'Pictures'
'Date';
/*
@media (min-width: 400px){
  grid-template-areas: 
'Avatar  Avatar' 
'Name  Name'
'Description  Description'
'Price  Location'
'Link  Link'
'Pictures  Pictures'
'.  Date';
grid-template-columns: auto;
} */
`
/* For future use with card-flips */

const CardBack = styled.div`
display: grid;
justify-items: center;
grid-gap: 15px;
grid-template-columns: minmax(1fr 350px);
grid-template-rows: auto;
`



const Avatar = styled.div`
border-radius: 50%;
grid-area: Avatar;
`
const Name = styled.div`
margin-top: 20px;
color: #a1a1a1;
font-weight: 700;
grid-area: Name;
justify-self: start;
`

const Description = styled.div`
grid-area: Description;
`

const Info = styled.div`
grid-area: Info;
display: grid;
grid-template-columns: repeat(2, 1fr);
& 
.info{
  font-weight: 700;
}
.left {
  float: left;
  justify-self: left;
}
.right {
  float: right;
  justify-self: right;
}
`

const Location = styled.div`
grid-area: Location;
justify-self: end;
& .location {
    font-weight: 700;
}
`
const CatLink = styled.div`
color: rgba(68,0,153,0.5);
grid-area: Link;
word-break: break-all;
`

const Posted = styled.div`
color:#a1a1a1;
grid-area: Date;
justify-self: end;
align-self: end;
position: absolute;
bottom: 10px;
right: 15px;
`

const PictureGalery = styled.div`
display: grid;
/* grid-auto-flow: column; */
grid-template-columns: repeat(4, 1fr);
grid-template-rows: auto;
grid-gap: 10px;
grid-area: Pictures;
`
const StyledTooltip = styled(props => (
  <Tooltip
    classes={{ popper: props.className, tooltip: "tooltip" }}
    {...props}
  />
))`
& .tooltip {
    background-color: papayawhip;
    color: #000;
    font-size: 20px;
    margin: 0px;
}
`

export {CardBackground, CardFront, Avatar, PictureGalery, StyledTooltip, Name, Info, Description, CatLink, Posted}